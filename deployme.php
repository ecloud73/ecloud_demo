<?php

namespace Tygh\Addons;

$addons = array(
    'ecloud_mpp',
);

define('AREA', 'A');

require(dirname(__FILE__) . '/init.php');

use Tygh\Registry;
use Tygh\Settings;
use Tygh\Addons\SchemesManager;
use Tygh\ExSimpleXmlElement;
use Tygh\Languages\Languages;
use Tygh\Snapshot;
use Tygh\Tools\Url;
use Tygh\SDAddonUpdater;

/**
 * Version 1.1.0
 * Class for updating installed module setting according to current version of the "addon.xml" file
 *
 */
class AddonUpdater {
    /**
     *
     * Addon ID
     * @var string
     */
    private $_addon_id;

    /**
     *
     * Show log
     * @var bool
     */
    public $show_log;

    /**
     *
     * Addon scheme
     * @var array
     */
    private $_addon_scheme;

    /**
     *
     * Language code in lower case, e.g. "en"
     * @var string
     */
    private $lang_code;

    /**
     *
     * Section data
     * @var array
     */
    private $_db_section;

    /**
     *
     * Section ID for addon
     * @var int
     */
    private $_addon_section_id;

    /**
     *
     * Section ID for current addon tab
     * @var int
     */
    private $_section_tab_id;

    /**
     *
     * Section ID for current addon setting
     * @var int
     */
    private $_section_setting_id;

    /**
     *
     * Section tabs from database
     * @var array
     */
    private $_db_subsections;

    /**
     *
     * Module options from database
     * @var array
     */
    private $_db_options;

    /**
     * 
     * New settings set
     * @var array
     */
    private $_xml_tabs;

    /**
     * 
     * Workflow logs
     * @var array
     */
    public $events;

    /**
     *
     * Event constants
     * @var string
     */
    const EVENT_ERROR  = 'e';
    const EVENT_UPDATE = 'u';
    const EVENT_CUSTOM = 'c';
    const EVENT_ADD    = 'a';
    const EVENT_DELETE = 'd';
    const EVENT_SKIP   = 's';

    /**
     * Empty constructor
     */
    public function __construct()
    {
        $this->lang_code = CART_LANGUAGE;
        $this->events = array();
    }

    /**
     * Central function for updating module settings
     *
     * @param string $addon_id Addon ID
     */
    public function update($addon_id, $params = array())
    {
        $this->_addon_id = $addon_id;
        $this->init_addon_xml_data();
        $this->init_addon_db_data();

        fn_echo('<strong style="font-size: 225%; color:blue;">' . $addon_id . '</strong><br />');

        $default_params = array(
            'settings'           => true,
            'language_variables' => true,
            'queries'            => true, // FIXME not supported
        );

        $params = array_merge($default_params, $params);

        if ($params['settings'] && !empty($this->_addon_section_id)) {
            fn_echo('<strong style="font-size: 200%; color:blue;">Settings</strong><br />');
            $this->updateXMLTabs();
            $this->deleteObsoleteSections();
            $this->_print_breaker();
        }

        if ($params['language_variables']) {
            fn_echo('<strong style="font-size: 200%; color:blue;">Language variables</strong><br />');
            $this->updateLanguageVariables();
            $this->_print_breaker();
        }

        if ($params['queries']) {
            $this->updateQueries();
        }
    }

    /**
     * Get addon section, tabs and settings
     */
    private function init_addon_db_data()
    {
        if (empty($this->_addon_id)) {
            $this->_log_error('empty', '$addon_id');
            return;
        }

        $this->_db_section = Settings::instance()->getSectionByName($this->_addon_id, Settings::ADDON_SECTION);

        if (empty($this->_db_section['section_id']) && !empty($this->_xml_tabs)) {
            // Create root settings section
            $update_section_parameters = array(
                'parent_id'    => 0,
                'edition_type' => $this->_addon_scheme->getEditionType(),
                'name'         => $this->_addon_scheme->getId(),
                'type'         => Settings::ADDON_SECTION,
            );

            $update_section_parameters['section_id'] = Settings::instance()->updateSection($update_section_parameters);

            if (empty($update_section_parameters['section_id'])) {
                $this->_log_error('db_exist', '$addon_id');
                return false;
            } else {
                $_id = 'Section:' . $update_section_parameters['name'];
                $this->_db_section = $update_section_parameters;
                $this->_log_update($this::EVENT_ADD, $_id, $update_section_parameters['section_id']);
            }
        }

        if (!empty($this->_db_section['section_id'])) {
            $this->_addon_section_id = $this->_db_section['section_id'];
            $this->_db_subsections = Settings::instance()->getSectionTabs($this->_addon_section_id, $this->lang_code);
            $this->_db_options = Settings::instance()->getList($this->_addon_section_id);
        }
    }

    /**
     * Get addon settings from current XML file - addon.xml
     */
    private function init_addon_xml_data()
    {
        if (empty($this->_addon_id)) {
            return;
        }

        $this->_addon_scheme = SchemesManager::getScheme($this->_addon_id);
        $this->_xml_tabs = $this->_addon_scheme->getSections();
    }

    /**
     * Update addon setting tabs
     */
    private function updateXMLTabs()
    {
        if (empty($this->_addon_section_id) || empty($this->_xml_tabs) || !is_array($this->_xml_tabs)) {
            return;
        }

        foreach ($this->_xml_tabs as $tab_index => $tab) {
            $this->updateXMLTab($tab, $tab_index);
        }
    }

    /**
     * Update particular setting tab and its subelements - settings + variants
     *
     * @param array $tab Setting tab data
     * @param int $tab_index Tab position in XML file
     */
    private function updateXMLTab($tab, $tab_index)
    {
        if (empty($this->_addon_section_id)) {
            return false;
        }

        if (empty($tab['id'])) {
            $this->_log_error('empty', '$tab[id]');
            return false;
        }

        $section_tab_id = 0;
        $found_subsection = array();

        if (!empty($this->_db_subsections)) {
            foreach ($this->_db_subsections as $tab_id => $subsection) {
                if ($tab['id'] == $tab_id) {
                    $section_tab_id = $subsection['section_id'];
                    $found_subsection = $subsection;
                    unset($this->_db_subsections[$tab_id]);
                    break;
                }
            }
        }

        $update_section_data = array(
            'parent_id'    => $this->_addon_section_id,
            'edition_type' => $tab['edition_type'],
            'name'         => $tab['id'],
            'position'     => $tab_index * 10,
            'type'         => isset($tab['separate']) ? Settings::SEPARATE_TAB_SECTION : Settings::TAB_SECTION,
        );

        $skip_db_query = false;

        if (!empty($section_tab_id)) {
            $skip_db_query = true;
            $ignore_fields = array('edition_type');
            foreach ($update_section_data as $field => $value) {
                if (in_array($field, $ignore_fields)) {
                    continue;
                }

                if (!isset($found_subsection[$field]) || $found_subsection[$field] != $value) {
                    $skip_db_query = false;
                }
            }

            $update_section_data['section_id'] = $section_tab_id;
        }

        if (!$skip_db_query) {
            $db_id = Settings::instance()->updateSection($update_section_data);
        }

        $_id = 'Tab:' . $tab['id'];

        if (!empty($db_id)) {
            if (empty($section_tab_id)) {
                $section_tab_id = $db_id;
                $this->_log_update($this::EVENT_ADD, $_id, $section_tab_id);
            } else {
                $this->_log_update($this::EVENT_UPDATE, $_id, $section_tab_id);
            }
        } elseif ($skip_db_query) {
            $this->_log_update($this::EVENT_SKIP, $_id, $section_tab_id);
        } else {
            $this->_log_error('db', $tab['id']);
            return false;
        }

        $this->_section_tab_id = $section_tab_id;
        fn_update_addon_settings_descriptions($section_tab_id, Settings::SECTION_DESCRIPTION, $tab['translations']);

        $this->updateXMLTabSettings($tab);

        return true;
    }

    /**
     * Update settings of particular addon tab
     *
     * @param array $tab Setting tab data
     */
    private function updateXMLTabSettings($tab)
    {
        if (empty($this->_section_tab_id) || empty($tab['id'])) {
            return false;
        }

        $settings = $this->_addon_scheme->getSettings($tab['id']);

        if (!empty($settings)) {
            foreach ($settings as $position => $setting) {
                $this->updateXMLTabSetting($tab, $setting, $position);
            }
        }
    }

    /**
     * Update particular settings of particular addon tab and its variants
     *
     * @param array $tab Setting tab data
     * @param array $setting Setting data
     * @param int $position Setting position
     */
    private function updateXMLTabSetting($tab, $setting, $position)
    {
        if (empty($this->_section_tab_id) || empty($tab['id'])) {
            return false;
        }

        if (empty($setting['id'])) {
            $this->_log_error('empty', $tab['id'] . ':$setting[id]');
            return false;
        }

        $existed_setting = array();
        $setting_id = 0;

        if (isset($this->_db_options[$tab['id']])) {
            // Search setting in current tab
            foreach ($this->_db_options[$tab['id']] as $_setting_id => $_setting) {
                if ($_setting['name'] == $setting['id']) {
                    $existed_setting = $_setting;
                    $setting_id = $_setting_id;
                    unset($this->_db_options[$tab['id']][$_setting_id]);
                    break;
                }
            }
        }

        if (empty($setting_id)) {
            // Search setting among other tabs
            foreach ($this->_db_options as $tab_id => $tab_settings) {
                if ($tab_id == $tab['id']) {
                    continue;
                }

                foreach ($tab_settings as $_setting_id => $_setting) {
                    if ($_setting['name'] == $setting['id']) {
                        $existed_setting = $_setting;
                        $setting_id = $_setting_id;
                        unset($this->_db_options[$tab_id][$_setting_id]);
                        break;
                    }
                }
            }
        }

        $skip_db_query = false;

        $update_section_data = array(
            'name'           => $setting['id'],
            'section_id'     => $this->_addon_section_id,
            'section_tab_id' => $this->_section_tab_id,
            'type'           => $setting['type'],
            'position'       => isset($setting['position']) ? $setting['position'] : $position * 10,
            'edition_type'   => $setting['edition_type'],
            'is_global'      => 'N',
            'handler'        => $setting['handler'],
        );

        if (!empty($setting_id)) {
            $skip_db_query = true;
            $ignore_fields = array();
            foreach ($update_section_data as $field => $value) {
                if (in_array($field, $ignore_fields)) {
                    continue;
                }

                if (!isset($existed_setting[$field]) || $existed_setting[$field] != $value) {
                    $skip_db_query = false;
                }
            }

            $update_section_data['object_id'] = $setting_id;
        }

        if (!$skip_db_query) {
            $db_id = Settings::instance()->update($update_section_data);
        }

        $_id = 'Setting:' . $setting['id'];
        if (!empty($db_id)) {
            if (empty($setting_id)) {
                $setting_id = $db_id;
                $this->_log_update($this::EVENT_ADD, $_id, $setting_id);
                Settings::instance()->updateValueById($setting_id, $setting['default_value']);
            } else {
                $this->_log_update($this::EVENT_UPDATE, $_id, $setting_id);
            }
        } elseif ($skip_db_query) {
            $this->_log_update($this::EVENT_SKIP, $_id, $setting_id);
        } else {
            $this->_log_error('db', $setting['id']);
            return false;
        }

        $this->_section_setting_id = $setting_id;
        fn_update_addon_settings_descriptions($setting_id, Settings::SETTING_DESCRIPTION, $setting['translations']);

        $this->updateXMLTabSettingVariants($setting, $existed_setting);

        return true;
    }

    /**
     * Update particular settings of particular addon tab and its variants
     *
     * @param array $setting Setting data
     * @param array $existed_setting Old database setting data
     */
    private function updateXMLTabSettingVariants($setting)
    {
        $existed_variants = $this->getSettingVariants($setting);

        if (!empty($setting['variants'])) {
            foreach ($setting['variants'] as $position => $variant) {
                if (empty($variant['id'])) {
                    $this->_log_error('empty', $setting['id'] . ':$variant[id]');
                    continue;
                }

                $variant_id = 0;
                $existed_variant = array();

                if (!empty($existed_variants)) {
                    foreach ($existed_variants as $key => $_variant) {
                        if ($_variant['name'] == $variant_id) {
                            $variant_id = $_variant['variant_id'];
                            $existed_variant = $_variant;
                            unset($existed_variants[$key]);
                            break;
                        }
                    }
                }

                $skip_db_query = false;

                $update_variant_data = array(
                    'object_id'  => $this->_section_setting_id,
                    'name'       => $variant['id'],
                    'position'   => isset($variant['position']) ? $variant['position'] : $position * 10,
                );

                if (!empty($variant_id)) {
                    $skip_db_query = true;
                    $ignore_fields = array();
                    foreach ($update_variant_data as $field => $value) {
                        if (in_array($field, $ignore_fields)) {
                            continue;
                        }

                        if (!isset($existed_variant[$field]) || $existed_variant[$field] != $value) {
                            $skip_db_query = false;
                        }
                    }

                    $update_variant_data['variant_id'] = $variant_id;
                }

                if (!$skip_db_query) {
                    $db_id = Settings::instance()->updateVariant($update_variant_data);
                }

                $_id = 'Variants:' . $setting['id'];
                if (!empty($db_id)) {
                    if (empty($variant_id)) {
                        $variant_id = $db_id;
                        $this->_log_update($this::EVENT_ADD, $_id, $variant_id);
                    } else {
                        $this->_log_update($this::EVENT_UPDATE, $_id, $variant_id);
                    }
                } elseif ($skip_db_query) {
                    $this->_log_update($this::EVENT_SKIP, $_id, $variant_id);
                } else {
                    $this->_log_error('db', $setting['id']);
                    return false;
                }
 
                if (!empty($variant_id)) {
                    fn_update_addon_settings_descriptions($variant_id, Settings::VARIANT_DESCRIPTION, $variant['translations']);
                }
            }
        }

        if (!empty($existed_variants)) {
            foreach ($existed_variants as $variant) {
                $res = Settings::instance()->removeVariant($variant['variant_id']);

                if ($res) {
                    $this->_log_delete($variant['name'], $variant['variant_id']);
                } else {
                    $this->_log_error('db', $variant['variant_id']);
                }
            }
        }
    }

    /**
     * Get setting variants from the database
     *
     * @param string $setting_id Setting ID name
     */
    private function getSettingVariants($setting)
    {
        if (empty($setting['id'])) {
            $this->_log_error('empty', '$setting[id]');
            return array();
        }

        $object_id = Settings::instance()->getId($setting['id'], $this->_addon_id);

        if (empty($object_id)) {
            $this->_log_error('empty', $setting['id'] . ':$object_id');
            return array();
        }

        $object_condition = db_quote('?:settings_variants.object_id = ?i AND', $object_id);

        $variants = db_get_array(
            "SELECT ?:settings_variants.*, ?:settings_descriptions.value, ?:settings_descriptions.object_type "
                . "FROM ?:settings_variants "
                . "INNER JOIN ?:settings_descriptions "
                ."ON ?:settings_descriptions.object_id = ?:settings_variants.variant_id AND object_type = ?s "
                . "WHERE ?p ?:settings_descriptions.lang_code = ?s ORDER BY ?:settings_variants.position"
            , Settings::VARIANT_DESCRIPTION, $object_condition, $this->lang_code
        );

        return $variants;
    }

    /**
     * Delete obsolete sections
     *
     */
    private function deleteObsoleteSections()
    {
        if (!empty($this->_db_subsections)) {
            foreach ($this->_db_subsections as $tab_id => $subsection) {
                $res = Settings::instance()->removeSection($subsection['section_id']);

                if ($res) {
                    $this->_log_delete($subsection['name'], $subsection['section_id']);
                } else {
                    $this->_log_error('db', $subsection['section_id']);
                }
            }
        }

        if (!empty($this->_db_options)) {
            foreach ($this->_db_options as $tab_id => $settings) {
                foreach ($settings as $_setting_id => $_setting) {
                    $res = Settings::instance()->removeById($_setting['object_id']);
                    $this->_log_delete($_setting['name'], $_setting['object_id']);
                }
            }
        }
    }
    
    private function updateQueries()
    {
        $currentVersion = db_get_field('SELECT version FROM ?:addons WHERE addon = ?s',
            $addon = $this->_addon_scheme->getId());
        $consideredVersion = $this->_addon_scheme->getVersion();

        // If addon.xml does not bring a new version, don't try to perform the upgrades
        if ($consideredVersion <= $currentVersion) {
            return;
        }

        $updateQueries = $this->_addon_scheme->_xml->xpath(
            "//queries/item[(@for='install' or not(@for))
            and @version <= $consideredVersion and @version > $currentVersion]");

        if ($updateQueries) {
            // Sort queries by version
            usort($updateQueries, function($a, $b) {
                return (string)$a['version'] > (string)$b['version'];
            });

            foreach ($updateQueries as $query) {
                $attributes = $query->attributes();
                $dbQuery = (string)$query;
                if (isset($attributes['type']) && (string) $attributes['type'] == 'file') {
                    if(file_exists($this->_addon_path . '/' . $dbQuery)) {
                        db_import_sql_file($this->_addon_path . '/' . $dbQuery, 16384, false);
                    }
                }
                else {
                    db_query($dbQuery);
                }
                $this->_log_update($this::EVENT_ADD, $dbQuery);
            }
        }

        db_query("UPDATE ?:addons SET version = ?s WHERE addon = ?s", $consideredVersion, $addon);
        $this->_log_update($this::EVENT_UPDATE, $addon,
            "Updated from $currentVersion to $consideredVersion");
    }

    /**
     * Restore language variables
     *
     */
    private function updateLanguageVariables()
    {
        $language_pack = array();
        $default_lang = $this->_addon_scheme->getDefaultLanguage();
        $tranlation_languages = fn_get_translation_languages();

        if (class_exists('Tygh\Addons\XmlScheme3')) {
            $language_variables = $this->_addon_scheme->getLanguageValues(false);
            foreach ($language_variables as $lang_var) {
                $language_pack[$lang_var['name']][$lang_var['lang_code']] = $lang_var['value'];
            }
        } else {
            $node = '//language_variables';
            $language_pack = array();

            foreach ($tranlation_languages as $lang_code => $_v) {
                if (!empty($this->_addon_scheme->_xml)) {
                    $current_langvars = $this->_addon_scheme->_xml->xpath($node . "/item[@lang='$lang_code']");
                } else {
                    $this->_log_error('empty', 'To fix it, make $_xml property "public" (instead of "private") for AXmlScheme - App/Tygh/Addons/AXmlScheme.php');
                    break;
                }

                foreach ($current_langvars as $lang_var) {
                    $id = (string) $lang_var['id'];
                    if (!isset($language_pack[$id])) {
                        $language_pack[$id] = array();
                    }

                    $language_pack[$id][$lang_code] = (string) $lang_var;
                }
            }
        }

        if (!$language_pack) {
            $this->_log_update($this::EVENT_SKIP, $this->_addon_id, 'No language variables');
            return;
        }

        $condition = db_quote(' AND name IN (?a)', array_keys($language_pack));
        $existed_vars = db_get_hash_multi_array('SELECT * FROM ?:language_values WHERE 1 ?p', array('name', 'lang_code', 'value'), $condition);
        $new_pack = array();

        foreach ($language_pack as $id => $lang_data) {
            if (!is_array($lang_data) || empty($lang_data)) {
                continue;
            }

            if (isset($lang_data[$default_lang])) {
                $default_value = $lang_data[$default_lang];
            } else {
                $default_value = $lang_data[key($lang_data)];
            }

            foreach ($tranlation_languages as $lang_code => $_v) {
                if (isset($lang_data[$lang_code])) {
                } else {
                    $lang_data[$lang_code] = $default_value;
                }
            }

            ksort($lang_data);

            foreach ($lang_data as $lang_code => $lang_var) {
                if (isset($existed_vars[$id][$lang_code]) && false) {
                    $this->_log_update($this::EVENT_SKIP, $id, $lang_code);
                } else {
                    $new_pack[] = array(
                        'lang_code' => $lang_code,
                        'name' => $id,
                        'value' => $lang_var,
                    );
                    $this->_log_update($this::EVENT_ADD, $id, $lang_code);
                }
            }
        }

        if ($new_pack) {
            db_query('REPLACE INTO ?:language_values ?m', $new_pack);
        }
    }

   /**
     * Add event data to storage
     *
     * @param string $type See event constants
     * @param array $data Event data
     */
    private function _add_log($type, $data) {
        static $count = 0;

        if (empty($type)) {
            return;
        }

        $count++;

        if (!isset($this->events[$type])) {
            $this->events[$type] = array();
        }

        $data['sequence'] = $count;

        $this->events[$type][$count] = $data;
        $this->_show_log($type, $data);
    }

   /**
     * Show log to the screen
     *
     * @param string $type See event constants
     * @param array $data Event data
     */
    public function _show_log($type, $data) {
        if (!$this->show_log) {
            return;
        }
                
        $type_matrix = array(
            $this::EVENT_ERROR => array(
                'text' => 'Error',
                'color' => 'red',
            ),
            $this::EVENT_UPDATE => array(
                'text' => 'Update',
                'color' => 'black',
            ),
            $this::EVENT_CUSTOM => array(
                'text' => 'Custom',
                'color' => 'grey',
            ),
            $this::EVENT_ADD => array(
                'text' => 'Add',
                'color' => 'blue',
            ),
            $this::EVENT_DELETE => array(
                'text' => 'Delete',
                'color' => 'green',
            ),
            $this::EVENT_SKIP => array(
                'text' => 'Skip',
                'color' => 'grey',
            )
        );

        $record = $data['sequence'] . '. ';
        $record .= '<span style="color:' . $type_matrix[$type]['color'] . ';">' . $type_matrix[$type]['text'] . ': ';

        if (!empty($data['secondary_type'])) {
            $record .= $data['secondary_type'] . ' ';
        }

        $record .= '<b>' . @$data['object_id'] . '</b> - ' . @$data['id'];
        $record .= ' ' . @$data['description'];
        $record .= '</span><br />';
        fn_echo($record);
    }

    /**
     * Log error event
     *
     * @param string $error_type Pre-defined additional type - 'empty' (Empty value), 'db' (Database error), 'db_exist' (Does not exist in database)
     * @param string $id Event object identifier
     * @param string $description Event description
     */
    private function _log_error($error_type, $id = '', $description = '') {
        $error_data = array(
            'secondary_type' => $error_type,
            'id'             => $id,
            'description'    => $description,
        );

        $this->_add_log($this::EVENT_ERROR, $error_data);
    }

    /**
     * Log update or add event
     * 
     * @param string $type 'a' (Add) or 'u' (Update)
     * @param string $object_id Object name identifier
     * @param string $id Event object identifier
     * @param string $description Event description
     */
    private function _log_update($type, $object_id, $id = '', $description = '') {
        $log_data = array(
            'object_id'   => $object_id,
            'id'          => $id,
            'description' => $description,
        );

        $this->_add_log($type, $log_data);
    }

    /**
     * Log update or add event
     * 
     * @param string $object_id Object name identifier
     * @param string $id Event object identifier
     * @param string $description Event description
     */
    private function _log_delete($object_id, $id = '', $description = '') {
        $type = $this::EVENT_DELETE;

        $log_data = array(
            'object_id'   => $object_id,
            'id'          => $id,
            'description' => $description,
        );

        $this->_add_log($type, $log_data);
    }

    /**
     * Output breaking line
     * 
     */
    public function _print_breaker()
    {
        fn_echo('<p style="color:blue;">' . str_repeat('*', 77) . '</p><br />');
    }

    /**
     * Get list of default modules for auto-update
     * 
     */
    public function getDefaultModuleList()
    {
        $default_modules = Registry::get('runtime_dev.addon_updater.modules');

        $condition = db_quote(" AND status = ?s AND addon LIKE ?l AND addon != ?s", 'A', 'sd_%', 'sd_addon_updater');
        $additional_modules = db_get_fields('SELECT addon FROM ?:addons WHERE 1 ?p', $condition);

        $default_modules = array_merge($default_modules, $additional_modules);

        return $default_modules;
    }
}

abstract class AXmlScheme
{
    /**
     * @var ExSimpleXmlElement
     */
    public $_xml;

    /**
     * @var array Available languages
     */
    private $languages;

    /**
     * Gets available languages
     * @return array languages list
     */
    public function getLanguages()
    {
        if (empty($this->languages)) {
            $this->languages = Languages::getAll();
        }

        return $this->languages;
    }

    /**
     * Returns array of types for addons setting
     * @return array
     */
    protected function _getTypes()
    {
        return array (
            'input' => 'I',
            'textarea' => 'T',
            'radiogroup' => 'R',
            'selectbox' => 'S',
            'password' => 'P',
            'checkbox' => 'C',
            'multiple select' => 'M',
            'multiple checkboxes' => 'N',
            'countries list' => 'X',
            'states list' => 'W',
            'file' => 'F',
            'info' => 'O',
            'header' => 'H',
            'selectable_box' => 'B',
            'template' => 'E',
            'permanent_template' => 'Z',
            'hidden' => 'D'
        );
    }

    /**
     * Creates instance of class
     *
     * @param $addon_xml ExSimpleXmlElement with addon scheme
     */
    public function __construct($addon_xml)
    {
        $this->_xml = $addon_xml;
    }

    /**
     * Returns text id of addon from xml
     * @return string
     */
    public function getId()
    {
        return (string) $this->_xml->id;
    }

    /**
     * Returns addons text name from xml.
     * @param  string $lang_code
     * @return string
     */
    public function getName($lang_code = CART_LANGUAGE)
    {
        $name = $this->_getTranslation($this->_xml, 'name', $lang_code);

        return ($name == '') ? (string) $this->_xml->name : $name;
    }

    /**
     * Returns addons text description from xml.
     * @param  string $lang_code
     * @return string
     */
    public function getDescription($lang_code = CART_LANGUAGE)
    {
        $description = $this->_getTranslation($this->_xml, 'description', $lang_code);

        return ($description == '') ? (string) $this->_xml->description : $description;
    }

    /**
     * Returns priority of addon from xml
     * @return int
     */
    public function getPriority()
    {
        return (isset($this->_xml->priority)) ? (int) $this->_xml->priority  : 0;
    }

    /**
     * Returns priority of addon from xml
     * @return string
     */
    public function getStatus()
    {
        $statuses = array(
            'active' => 'A',
            'disabled' => 'D'
        );

        return isset($this->_xml->status) ? $statuses[(string) $this->_xml->status] : 'D';
    }

    /**
     * Returns array of addon's ids
     * @return array
     */
    public function getDependencies()
    {
        return (isset($this->_xml->dependencies)) ? explode(',', (string) $this->_xml->dependencies) : array();
    }

    /**
     * Returns unmanaged status
     * @return boolean
     */
    public function getUnmanaged()
    {
        return (isset($this->_xml->unmanaged));
    }

    /**
     * Return assray of names of conflicted addons
     */
    public function getConflicts()
    {
        $conflicts = array();
        foreach ($this->_xml->xpath('//conflicts') as $addon) {
            $conflicts[] = (string) $addon;
        }

        return $conflicts;
    }

    /**
     * Returns array of editions
     * @return array
     */
    public function autoInstallFor()
    {
        return (isset($this->_xml->auto_install)) ? explode(',', (string) $this->_xml->auto_install) : array();
    }

    /**
     * Returns comma separated list of editions for addon section
     * @return string
     */
    public function getEditionType()
    {
        return $this->_getEditionType($this->_xml->settings);
    }

    /**
     * Returns way how will be displayed addon settings list.
     * popup - in popup box
     * separate - in new window
     * @return string
     */
    public function getSettingsLayout()
    {
        return "popup";
    }

    /**
     * Executes queries from addon scheme.
     *
     * @param  string $mode
     * @param  string $addon_path
     * @return bool
     */
    public function processQueries($mode, $addon_path)
    {
        Registry::set('runtime.database.skip_errors', true);

        $languages = $this->getLanguages();
        $queries = $this->getQueries($mode);

        $lang_queries = array();

        if (!empty($queries) && is_array($queries)) {
            foreach ($queries as $query) {
                if (!empty($query['lang']) && !empty($query['table'])) {
                    $lang_queries[(string) $query['table']][(string) $query['lang']][] = $query;
                } else {
                    $this->_executeQuery($query, $addon_path);
                }
            }
        }

        $default_lang = $this->getDefaultLanguage();
        foreach ($lang_queries as $table_name => $queries) {
            // Check and execute default language queries
            if (isset($queries[$default_lang])) {
                // Actions with default language
                foreach ($queries[$default_lang] as $default_query) {
                    $this->_executeQuery($default_query, $addon_path);

                    // Clone default values to all other languages
                    foreach ($languages as $lang_code => $lang_data) {
                        fn_clone_language_values((string) $default_query['table'], $lang_code, (string) $default_query['lang']);
                    }
                }
            }

            // execute other languages queries
            foreach ($languages as $lang_code => $lang_data) {
                if (isset($queries[$lang_code])) {
                    foreach ($queries[$lang_code] as $query) {
                        $this->_executeQuery($query, $addon_path);
                    }
                }
            }
        }

        Registry::set('runtime.database.skip_errors', false);

        $errors = Registry::get('runtime.database.errors');
        if (!empty($errors)) {
            $error_text = '';
            foreach ($errors as $error) {
                $error_text .= '<br/>' . $error['message'] . ': <code>'. $error['query'] . '</code>';
            }
            $notification_text = ($mode == 'uninstall') ? __('addon_uninstall_sql_error') : __('addon_sql_error');
            fn_set_notification('E', $notification_text, $error_text);

            Registry::set('runtime.database.errors', array());

            return false;
        } else {
            return true;
        }
    }

    /**
     * Executes query from addon xml scheme
     * @return bool always true
     */
    private function _executeQuery($query, $addon_path)
    {
        if (isset($query['type']) && (string) $query['type'] == 'file') {
            db_import_sql_file($addon_path . '/' . (string) $query, 16384, false, 1, true);
        } else {
            db_query((string) $query);
        }

        return true;
    }

    /**
     * Returns tab order
     * @return string
     */
    public function getTabOrder()
    {
        return (isset($this->_xml->tab_order)) ? $this->_xml->tab_order : 'append';
    }

    /**
     * Returns addon promo status
     * @return bool
     */
    public function isPromo()
    {
        $addon_name = (string) $this->_xml->id;

        return !fn_check_addon_snapshot($addon_name);
    }

    /**
     * Checks if addon has custom icon
     * @return bool
     */
    public function hasIcon()
    {
        return (isset($this->_xml->has_icon)) ? (bool) $this->_xml->has_icon : false;
    }

    /**
     * Returns addon promo status
     * @return bool
     */
    public function getVersion()
    {
        return (isset($this->_xml->version)) ? (string) $this->_xml->version : '';
    }

    /**
     * Returns addon supplier
     * @return string
     */
    public function getSupplier()
    {
        $result = '';

        if (isset($this->_xml->supplier)) {
            $result = (string) $this->_xml->supplier;
        } elseif ($this->isCoreAddon()) {
            $result = 'Simtech';
        }

        return $result;
    }


    /**
     * Return addon supplier link
     * @return null|string http://example.com
     */
    public function getSupplierLink()
    {
        $url = isset($this->_xml->supplier_link) ? (string) $this->_xml->supplier_link : '';

        if (!empty($url) && Url::isValid($url)) {
            return $url;
        }

        return null;
    }

    /**
     * Returns edition type for this node.
     * If in this node has no edition type returns edition type of it's parent.
     * If for all parents of this node has no edition type returns ROOT.
     * @param  \SimpleXMLElement|array $xml_node
     * @return string
     */
    public function _getEditionType($xml_node)
    {
        $edition_type = 'ROOT'; // Set default value of edition type

        if (isset($xml_node['edition_type'])) {
            $edition_type = (string) $xml_node['edition_type'];
        } else { // Try to take parent edition type
            if (is_object($xml_node)) {
                $parent = $xml_node->xpath('parent::*');
                if (is_array($parent)) {
                    $parent = current($parent);
                    if (!empty($parent)) {
                        $edition_type = $this->_getEditionType($parent);
                    }
                }
            }
        }

        return $edition_type;
    }

    /**
     * Returns translations of description and addon name.
     * @return array|bool
     */
    public function getAddonTranslations()
    {
        return $this->_getTranslations($this->_xml);
    }

    public function callCustomFunctions($action)
    {
        // Execute custom functions
        if (isset($this->_xml->functions)) {
            Registry::set('runtime.database.skip_errors', true);

            $addon_name = (string) $this->_xml->id;
            // Include func.php file of this addon
            if (is_file(Registry::get('config.dir.addons') . $addon_name . '/func.php')) {
                require_once(Registry::get('config.dir.addons') . $addon_name . '/func.php');

                if (is_file(Registry::get('config.dir.addons') . $addon_name . '/config.php')) {
                    require_once(Registry::get('config.dir.addons') . $addon_name . '/config.php');
                }

                foreach ($this->_xml->functions->item as $v) {
                    if (($action == 'install' && !isset($v['for'])) || (string) $v['for'] == $action) {
                        if (function_exists((string) $v)) {
                            call_user_func((string) $v, $v, $action);
                        }
                    }
                }
            }

            Registry::set('runtime.database.skip_errors', false);

            $errors = Registry::get('runtime.database.errors');
            if (!empty($errors)) {
                $error_text = '';
                foreach ($errors as $error) {
                    $error_text .= '<br/>' . $error['message'] . ': <code>'. $error['query'] . '</code>';
                }
                $notification_text = ($action == 'uninstall') ? __('addon_uninstall_sql_error') : __('addon_sql_error');
                fn_set_notification('E', $notification_text, $error_text);

                Registry::set('runtime.database.errors', array());

                return false;
            }
        }

        return true;
    }

    /**
     * Uninstall all langvars
     */
    public function uninstallLanguageValues()
    {
        $node = $this->_getLangVarsSectionName();
        $langvars = $this->_xml->xpath($node . '/item');
        if (!empty($langvars) && is_array($langvars)) {
            foreach ($langvars as $langvar) {
                db_query("DELETE FROM ?:language_values WHERE name = ?s", (string) $langvar['id']);

                if (fn_allowed_for('ULTIMATE')) {
                    db_query("DELETE FROM ?:ult_language_values WHERE name = ?s", (string) $langvar['id']);
                }
            }
        }
    }

    /**
     * Install all langvars from addon xml scheme
     */
    public function getLanguageValues($only_originals = false)
    {
        $language_variables = array();
        $node = $this->_getLangVarsSectionName();
        $default_lang = $this->getDefaultLanguage();

        $original_langvars = (array) $this->_xml->xpath($node . "/item[@lang='en']");
        $_original = array();

        foreach ($original_langvars as $_v) {
            $_original[(string) $_v['id']] = (string) $_v;
        }

        $default_langvars = $this->_xml->xpath($node . "/item[@lang='$default_lang']");
        if (!empty($default_langvars)) {
            // Fill all languages by default laguage values
            foreach ($this->getLanguages() as $lang_code => $_v) {
                // Install default
                foreach ($default_langvars as $lang_var) {
                    $original = isset($_original[(string) $lang_var['id']]) ? $_original[(string) $lang_var['id']] : (string) $lang_var;

                    if ($only_originals) {
                        $language_variables[] = array(
                            'msgctxt' => 'Languages' . \I18n_Pofile::DELIMITER . (string) $lang_var['id'],
                            'msgid' => $original,
                        );

                    } else {
                        $language_variables[] = array(
                            'lang_code' => $lang_code,
                            'name' => (string) $lang_var['id'],
                            'value' => (string) $lang_var,
                        );
                    }
                }

                if ($lang_code != $default_lang) {
                    $current_langvars = $this->_xml->xpath($node . "/item[@lang='$lang_code']");
                    if (!empty($current_langvars)) {
                        foreach ($current_langvars as $lang_var) {
                            $original = isset($_original[(string) $lang_var['id']]) ? $_original[(string) $lang_var['id']] : (string) $lang_var;

                            if ($only_originals) {
                                $language_variables[] = array(
                                    'msgctxt' => 'Languages' . \I18n_Pofile::DELIMITER . (string) $lang_var['id'],
                                    'msgid' => $original,
                                );

                            } else {
                                $language_variables[] = array(
                                    'lang_code' => $lang_code,
                                    'name' => (string) $lang_var['id'],
                                    'value' => (string) $lang_var,
                                );
                            }
                        }
                    }
                }
            }
        }

        return $language_variables;
    }

    /**
     * Gets original values for language-dependence name/description
     *
     * @return array Original values
     */
    public function getOriginals()
    {
        return false;
    }

    /**
     * Gets path to PO translation for specified language
     *
     * @param  string      $lang_code 2-letters language identifier
     * @return string|bool Path to file if exists of false otherwise
     */
    public function getPoPath($lang_code)
    {
        return false;
    }

    /**
     * Returns one translation for some node for some language
     * @param $node
     * @param string $for
     * @param $lang_code
     * @return string
     */
    protected function _getTranslation($node, $for = '', $lang_code= CART_LANGUAGE)
    {
        $name = '';

        if (isset($node->translations)) {
            foreach ($node->translations->item as $item) {
                $a = isset($item['for']) && $for != '';
                $b = (string) $item['for'] == $for;
                $c = (string) $item['lang'] == $lang_code;
                if ($c && ($a && $b || !$a)) {
                    $name = (string) $item;
                }
            }
        }

        return $name;
    }

    /**
     * Returns all translations for xml_node for all installed laguages if it is presents in addon xml
     * @param  \SimpleXMLElement|array $xml_node
     * @return array|bool
     */
    protected function _getTranslations($xml_node)
    {
        $translations = array();

        $default_language = $this->getDefaultLanguage();

        // Generate id from attribute or property
        if (isset($xml_node['id'])) {
            $id = (string) $xml_node['id'];
        } elseif (isset($xml_node->id)) {
            $id = (string) $xml_node->id;
        } else {
            return false;
        }

        $default_translation = array(
            'lang_code' => $default_language,
            'name' => $id,
            'value' => (string) $xml_node->name,
            'tooltip' => isset($xml_node->tooltip) ? (string) $xml_node->tooltip : '',
            'description' => isset($xml_node->description) ? (string) $xml_node->description : '',
        );

        // Fill all languages by default laguage values
        foreach ($this->getLanguages() as $lang_code => $_v) {
            $value = $xml_node->xpath("translations/item[(not(@for) or @for='name') and @lang='$lang_code']");
            $tooltip = $xml_node->xpath("translations/item[@for='tooltip' and @lang='$lang_code']");
            $description = $xml_node->xpath("translations/item[@for='description' and @lang='$lang_code']");
            if (!empty($value) || !empty($default_translation['value'])) {
                $translations[] = array(
                    'lang_code' =>  $lang_code,
                    'name' => $default_translation['name'],
                    'value' => !empty($value) && is_array($value) ? (string) current($value) : $default_translation['value'],
                    'tooltip' => !empty($tooltip) && is_array($tooltip) ? (string) current($tooltip) : $default_translation['tooltip'],
                    'description' => !empty($description) && is_array($description) ? (string) current($description) : $default_translation['description'],
                );
            }
        }

        return $translations;
    }

    /**
     * Returns array of setting item data from xml node
     * @param $xml_node
     * @return array
     */
    protected function _getSettingItem($xml_node)
    {
        if (isset($xml_node['id'])) {
            $_types = $this->_getTypes();
            $setting = array(
                'edition_type' =>  $this->_getEditionType($xml_node),
                'id' => (string) $xml_node['id'],
                'name' => (string) $xml_node->name,
                'type' => isset($_types[(string) $xml_node->type]) ? $_types[(string) $xml_node->type] : '',
                'translations' => $this->_getTranslations($xml_node),
                'default_value' => isset($xml_node->default_value) ? (string) $xml_node->default_value : '',
                'variants' => $this->_getVariants($xml_node),
                'handler' => isset($xml_node->handler) ? (string) $xml_node->handler : '',
                'parent_id' => isset($xml_node['parent_id']) ? (string) $xml_node['parent_id'] : '',
                'original' => '',
            );

            return $setting;
        } else {
            return array();
        }
    }

    /**
     * Returns array of variants of setting item from xml node
     * @param $xml_node
     * @return array
     */
    protected function _getVariants($xml_node)
    {
        $variants = array();
        if (isset($xml_node->variants)) {
            foreach ($xml_node->variants->item as $variant) {
                $variants[] = array(
                    'id' => (string) $variant['id'],
                    'name' => (string) $variant->name,
                    'translations' => $this->_getTranslations($variant),
                    'original' => '',
                );
            }
        }

        return $variants;
    }

    /**
     * Returns array of language variables of addon.
     *
     * @abstract
     * @return array
     */
    abstract protected function _getLangVarsSectionName();

    /**
     * Returns array of settings sections of addon.
     * In current version of cart it is tabs on addon's update settings page
     *
     * @abstract
     * @return array
     */
    abstract public function getSections();

    /**
     * Returns array of settings on section
     * @abstract
     * @param $section_id
     * @return array
     */
    abstract public function getSettings($section_id);

    /**
     * Returns array of SQL queries
     * @abstract
     * @param  string $mode May be install or uninstall
     * @return array
     */
    abstract protected function getQueries($mode = '');

    /**
     * Returns 2digits lang code
     * @abstract
     * @return string
     */
    abstract public function getDefaultLanguage();

    /**
     * Magic method for _serialize
     * @return array
     */
    public function __sleep ()
    {
        $this->_xml = $this->_xml->asXML();

        return array('_xml');
    }

    /**
     * Return flag is core addon
     * @return bool
     */
    public function isCoreAddon()
    {
        $core_addons = Snapshot::getCoreAddons();

        return in_array($this->getId(), $core_addons);
    }
}


$addon_updater_class = new AddonUpdater();
$addon_updater_class->show_log = true;

foreach ($addons as $addon) {
    $addon_updater_class->update($addon);
}

