<?php

use Tygh\Registry;
use Tygh\DataKeeper;

if (Registry::get('config.demo_mode')) {

    $disabled = array(
        'templates',
        'datakeeper',
        'file_editor',
        'documents'
    );

    if (in_array($controller, $disabled)) {
        return array(CONTROLLER_STATUS_REDIRECT, 'index.index');
    }

    if ($mode == 'restore' && $_REQUEST['cron_key'] == 'be-happy') {
        if (!empty($_REQUEST['backup_file'])) {
            $restore_result = DataKeeper::restore($_REQUEST['backup_file']);
            if ($restore_result === true) {
                fn_set_notification('N', __('notice'), __('done'));
            } elseif ($restore_result === false) {
            } else {
                fn_set_notification('E', __('error'), $restore_result);
            }
        }

        fn_print_die('done');
    }
}

