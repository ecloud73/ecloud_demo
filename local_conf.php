<?php

$config['demo_mode'] = true;

if (AREA == 'A') {
    $config['demo_username'] = 'admin@admin.com';
    $config['demo_password'] = 'admin';
} else {
    $config['demo_username'] = 'customer@example.com';
    $config['demo_password'] = 'customer';
}